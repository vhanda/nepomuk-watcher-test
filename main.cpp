#include <QtCore/QCoreApplication>
#include <QtCore/QTimer>

#include <Soprano/Statement>
#include <Soprano/Node>
#include <Soprano/Model>
#include <Soprano/QueryResultIterator>
#include <Soprano/StatementIterator>
#include <Soprano/NodeIterator>
#include <Soprano/PluginManager>

#include <Nepomuk/Resource>
#include <Nepomuk/ResourceManager>
#include <Nepomuk/Variant>
#include <Nepomuk/File>
#include <Nepomuk/Tag>

#include <Nepomuk/Types/Property>
#include <Nepomuk/Types/Class>

#include <Nepomuk/Query/Query>
#include <Nepomuk/Query/FileQuery>
#include <Nepomuk/Query/ComparisonTerm>
#include <Nepomuk/Query/LiteralTerm>
#include <Nepomuk/Query/ResourceTerm>
#include <Nepomuk/Query/QueryServiceClient>
#include <Nepomuk/Query/ResourceTypeTerm>
#include <Nepomuk/Query/QueryParser>
#include <Nepomuk/Query/Result>

#include <KDebug>

// Vocabularies
#include <Soprano/Vocabulary/RDF>
#include <Soprano/Vocabulary/RDFS>
#include <Soprano/Vocabulary/NRL>
#include <Soprano/Vocabulary/NAO>

#include <Nepomuk/Vocabulary/NIE>
#include <Nepomuk/Vocabulary/NFO>
#include <Nepomuk/Vocabulary/NMM>
#include <Nepomuk/Vocabulary/NCO>
#include <Nepomuk/Vocabulary/PIMO>

#include <KUrl>
#include <KJob>
#include <iostream>

#include <nepomuk/datamanagement.h>
#include <nepomuk/resourcewatcher.h>

using namespace Soprano::Vocabulary;
using namespace Nepomuk::Vocabulary;

class App : public QObject {
    Q_OBJECT
public slots:
    void main();
    void propertyAdded(const Nepomuk::Resource& res, const Nepomuk::Types::Property& prop,
                       const QVariant& value);
    void propertyRemoved(const Nepomuk::Resource& res, const Nepomuk::Types::Property& prop,
                         const QVariant& value);
    void propertyChanged(const Nepomuk::Resource& res, const Nepomuk::Types::Property& prop,
                         const QVariantList &oldList, const QVariantList &newList);
public:
    App() {
        QTimer::singleShot( 0, this, SLOT(main()) );
        //Nepomuk::ResourceWatcher* watcher = new Nepomuk::ResourceWatcher( this );
        Nepomuk::Resource res(QUrl("file:///home/vishesh/TODO"));
        kDebug() << "Resource Uri: " << res.resourceUri();
        //watcher->addResource( res );
        //watcher->start();
        kDebug() << "Starting watcher";

        kDebug() << "Numeric Rating: " << res.rating();
        //connect( watcher, SIGNAL(propertyAdded(Nepomuk::Resource, Nepomuk::Types::Property,QVariant)),
        //         this, SLOT(propertyAdded(Nepomuk::Resource, Nepomuk::Types::Property,QVariant)) );
        //connect( watcher, SIGNAL(propertyRemoved(Nepomuk::Resource, Nepomuk::Types::Property,QVariant)),
        //         this, SLOT(propertyRemoved(Nepomuk::Resource,Nepomuk::Types::Property,QVariant)) );
        //connect( watcher, SIGNAL(propertyChanged(Nepomuk::Resource, Nepomuk::Types::Property, QVariantList, QVariantList)),
        //         this, SLOT(propertyChanged(Nepomuk::Resource, Nepomuk::Types::Property, QVariantList, QVariantList)) );

        KJob* job = Nepomuk::setProperty( QList<QUrl>() << res.resourceUri(),
                                          NAO::numericRating(),
                                          QVariantList() << QVariant(1) );
        job->exec();
        kDebug() << job->errorString();

        kDebug() << "New rating: " << res.rating();
    }
};

int main( int argc, char ** argv ) {
    if( Nepomuk::ResourceManager::instance()->init() ) {
        qDebug() << "Nepomuk isn't running";
        return 0;
    }

    KComponentData component( QByteArray("nepomuk-test") );
    QCoreApplication app( argc, argv );

    App a;
    app.exec();
}

void App::main()
{
    // --- CODE
/*
    using namespace Nepomuk;
    using namespace Nepomuk::Vocabulary;

    Query::ResourceTypeTerm typeTerm( PIMO::Project() );
    Query::Query query( typeTerm );

    // Incase you want to see the sparql
    kDebug() << query.toSparqlQuery();

    QList< Query::Result > results = Query::QueryServiceClient::syncQuery( query );
    foreach( Query::Result r, results ) {
            Nepomuk::Resource res( r.resource() );
        kDebug() <<  res.resourceUri() << " " << res.genericLabel();
    }*/

    //QCoreApplication::instance()->quit();
}

void App::propertyAdded(const Nepomuk::Resource& res, const Nepomuk::Types::Property& prop, const QVariant& value)
{
    kDebug() << res;
    kDebug() << prop;
    kDebug() << value;
}

void App::propertyRemoved(const Nepomuk::Resource& res, const Nepomuk::Types::Property& prop, const QVariant& value)
{
    kDebug() << res;
    kDebug() << prop;
    kDebug() << value;
}

void App::propertyChanged(const Nepomuk::Resource& res, const Nepomuk::Types::Property& prop, const QVariantList& oldList, const QVariantList& newList)
{
    kDebug() << res;
    kDebug() << prop;
    kDebug() << oldList;
    kDebug() << newList;
}

#include "main.moc"
